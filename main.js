let gallery = [...document.querySelectorAll("img")];

let buttonPlay = document.createElement('button');
let buttonStop = document.createElement('button');
let content = document.querySelector('.content');

buttonStop.className = "play";
buttonStop.innerText = "Прекратить";
buttonPlay.className = "stop-playing";
buttonPlay.innerText = "Возобновить показ";

content.append(buttonStop);
content.append(buttonPlay);

function displayImage(gallery) {
    let i = gallery.map(function (el) {
        return el.className;
    }).indexOf('slide');
    if (i === gallery.length - 1) {
        gallery[i].classList.toggle('hide');
        i = 0;
        gallery[i].classList.toggle('hide');
    } else {
        gallery[i].classList.toggle('hide');
        gallery[i + 1].classList.toggle('hide');
    }
}

let isPlaying = true;

buttonPlay.addEventListener('click', function () {
    if (!isPlaying) {
        restart = setInterval(milisecCountDown, 25);
        isPlaying = true;
    }
});


buttonStop.addEventListener('click', function () {
    clearInterval(restart);
    isPlaying = false;
});

let timer = document.createElement('div');
$(timer).addClass('timer-styles');

$("body").append($(timer));

let seconds = 9;
let miliseconds = 1000;

let restart = setInterval(milisecCountDown, 25);

function milisecCountDown() {
    if (miliseconds > 0) {
        miliseconds = miliseconds - 25;
        timer.innerText = `${seconds}:${miliseconds}`;
    } else if (seconds > 0) {
        miliseconds = 1000;
        seconds--;
        timer.innerText = `${seconds}:${miliseconds}`;
    } else {
        displayImage(gallery);
        seconds = 9;
        miliseconds = 1000;
    }
}